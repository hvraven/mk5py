from __future__ import annotations

import asyncio
import typing
from typing import Optional

from mk5.errors import MessageError
from mk5.protocol import MK5Connection


class MK5:
    def __init__(self, connection: MK5Connection):
        self._conn = connection

    @classmethod
    async def init_serial(cls, port: str, baudrate: int = 115_200, **kwargs) -> MK5:
        conn = await MK5Connection.init_serial(port, baudrate=baudrate,
                                               **kwargs)
        return cls(conn)

    def close(self):
        self._conn.close()

    async def get_version_information(self) -> (int, int, int, int, int):
        """Retrieves the firmware, library and main board versions

        :returns
          a tuple of the firmware major, minor, and patch versions, followed
          by the library version and the main board version
        """
        resp = await self._conn.query("BGFVN")
        return self._convert_response(resp, int, int, int, int, int)

    async def get_offset_values(
        self,
    ) -> (
        (float, float),
        (float, float),
        (float, float),
        (float, float),
        (float, float),
        (float, float),
    ):
        """get the offset values

        :returns
          tuples of the coarse and fine compensation values for the
          x,y, and z axes for sensors 1, followed by the data for sensor 2
        """
        resp = await self._conn.query("BGSOV")
        values = self._convert_response(resp, *([float] * 12))

        return tuple(zip(values[:6], values[6:]))

    async def set_loop_status(
        self,
        ac_loop: Optional[bool] = None,
        dc_loop: Optional[bool] = None,
        selective_frequency_loop: Optional[bool] = None,
        overall_active: Optional[bool] = None,
    ):
        old_status = await self.get_loop_status()

        ac_loop = old_status[0] if ac_loop is None else ac_loop
        dc_loop = old_status[1] if dc_loop is None else dc_loop
        selective_frequency_loop = (
            old_status[2]
            if selective_frequency_loop is None
            else selective_frequency_loop
        )
        overall_active = old_status[3] if overall_active is None else overall_active

        status = (
            ac_loop + dc_loop * 2 + selective_frequency_loop * 4 + overall_active * 8
        )
        await self._conn.query("TSLST", status)

    async def get_loop_status(self) -> (bool, bool, bool, bool):
        """query the status of the different loops

        :returns
          the status of the different loops in the following order:
          AC, DC, SelectiveFrequencyDC, OverallActive
        """
        resp = await self._conn.query("TGLST")
        status = self._convert_response(resp, int)[0]
        return bool(status & 1), bool(status & 2), bool(status & 4), bool(status & 8)

    @staticmethod
    def _convert_response(parts: str, *types: typing.Type):
        """split a response to parts and convert to the specified types"""
        if len(parts) != len(types):
            raise MessageError(
                "invalid number of response entries, expected "
                f"{len(types)}, got {len(parts)}"
            )
        return tuple((t(p) for t, p in zip(types, parts)))
