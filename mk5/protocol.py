"""implementation of the MK5 serial protocol

described in the MK5 Software User Manual
"""
from __future__ import annotations

import asyncio
import functools
import logging
import operator

import serial_asyncio

from mk5.errors import ChecksumError, DeviceError, ProtocolError

log = logging.getLogger(__name__)


class MK5Connection:
    def __init__(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        self.reader = reader
        self.writer = writer
        self._crl: int = 0
        self._open_calls: dict[int, asyncio.Future] = {}

        self._runner = asyncio.ensure_future(self._process_messages())

    def stop(self):
        self._runner.cancel()

    @classmethod
    async def init_serial(cls, port: str, **kwargs) -> MK5Connection:
        reader, writer = await serial_asyncio.open_serial_connection(
            url=f"alt://{port}", **kwargs
        )
        return MK5Connection(reader, writer)

    def close(self):
        self.writer.close()

    async def query(self, command: str, *args, timeout=1) -> str:
        fut = await self.send(command, *args)
        return await asyncio.wait_for(fut, timeout=timeout)

    async def send(self, command: str, *args) -> asyncio.Future:
        crl = self._next_crl()
        fut = asyncio.Future()
        self._open_calls[crl] = fut
        msg = prepare_message(prepare_command(crl, command, *args))
        self.writer.write(msg)
        await self.writer.drain()
        return fut

    async def _process_messages(self):
        print("starting")
        while True:
            try:
                msg = await self.reader.readuntil(b"\r\n\r\r")
                log.debug("MK5Connection: received message: %s", msg)
                crl, command, args = parse_command(parse_response(msg))
                fut = self._open_calls.get(crl, None)
                if fut is None:
                    log.error(
                        "MK5Connection: received messages with unknown " "message id "
                    )
                    continue

                fut.set_result(args)
            except asyncio.IncompleteReadError:
                log.msg("stream got closed")
                # at eof
                return
            except Exception as e:
                print(e)
                log.exception(e)

    def _next_crl(self) -> int:
        crl = self._crl
        self._crl = (self._crl + 1) % 256
        return crl


def prepare_command(crl: int, command: str, *args) -> str:
    return " ".join((f"{crl:02x}", command, *map(str, args)))


def parse_command(data: str) -> (int, str, list[str]):
    sepd = data.split(" ")
    if len(sepd) < 3:
        raise ProtocolError("not enough elements in response " + data)
    crl = sepd[0]
    status = sepd[1]
    if int(status) != 0:
        raise DeviceError(f"device returned status code {status}")
    command = sepd[2]
    args = sepd[3:]
    return int(crl, 16), command, args


def prepare_message(data: str) -> bytes:
    if len(data) + 3 > 255:
        raise ProtocolError("message data may not be longer than 252 bytes")
    msg_id = "~"
    msg = f":{len(data) + 3:02x}{msg_id}{data}".encode("ascii")
    crc = functools.reduce(operator.xor, msg[1:])
    suffix = f"{crc:02x}\r".encode("ascii")
    return msg + suffix


def parse_response(msg: bytes) -> str:
    status = int(msg[4:5], 36)
    if status != 0:
        raise ProtocolError(f"got response status {status} in message: {msg}")
    length = int(msg[1:3], 16)
    if length > len(msg) - 7:
        raise ProtocolError("got incomplete message or invalid length")
    if msg[-6:-4] != b"##":
        our_crc = functools.reduce(operator.xor, msg[1:-6])
        sent_crc = int(msg[-6:-4], 16)
        if our_crc != sent_crc:
            raise ChecksumError(
                f"message checksum does not match (sent: f{msg[-6:-4]}, computed: {our_crc:02x}"
            )
    return msg[5 : length + 1].decode()
