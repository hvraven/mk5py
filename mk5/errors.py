class ProtocolError(RuntimeError):
    pass


class ChecksumError(ProtocolError):
    pass


class MessageError(ProtocolError):
    """retrieved an invalid / unexpected message"""


class DeviceError(RuntimeError):
    pass
