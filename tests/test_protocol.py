import pytest

from mk5.protocol import prepare_message


@pytest.fixture(params=["a", "ab", "abcc", "L" * 252])
def data(request):
    return request.param


def test_encoding():
    data = "test"
    msg = prepare_message(data)
    assert type(msg) == bytes


def test_ascii_only():
    with pytest.raises(UnicodeError):
        prepare_message("tüst")


def test_fixed_characters(data):
    msg = prepare_message(data)
    assert type(msg) == bytes
    assert msg[0] == 58
    assert msg[-1] == 13


def test_length(data):
    msg = prepare_message(data)
    msg_length = int(msg[1:3], 16)
    assert msg_length == len(msg) - 4 == len(data) + 3
