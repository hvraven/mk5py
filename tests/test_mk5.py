async def test_firmware_version(mk5):
    mk5.inject_reply(":1B0000 00 BGFVN 1 1 1 41 313E\r\n\r\r")
    info = await mk5.get_version_information()
    print(info)
    assert info[0] == 1
    assert info[1] == 1
    assert info[2] == 1
    assert info[3] == 41
    assert info[4] == 31


async def test_loop_status(mk5):
    mk5.inject_reply(":120000 00 TGLST 15##\r\n\r\r")
    status = await mk5.get_loop_status()
    print(status)
    assert len(status) == 4
    for stat in status:
        assert type(stat) == bool
        assert stat


async def test_offset_values(mk5):
    mk5.inject_reply(":AB0000 00 BGSOV -4.44457E+03 -1.05635E+04 +8.37278E+01 -7.74810E+01 -3.40531E+01 -6.43413E+01 +4.79923E+03 +6.99381E+03 +1.00050E+00 +3.95729E+01 +1.83336E+01 +3.26796E+01##\r\n\r\r")
    offsets = await mk5.get_offset_values()
    print(offsets)
    assert len(offsets) == 6
    for axis in offsets:
        assert len(axis) == 2
        coarse, fine = axis
        assert type(coarse) == float
        assert type(fine) == float
