import asyncio
from unittest.mock import Mock, AsyncMock

import pytest

from mk5.device import MK5
from mk5.protocol import MK5Connection


@pytest.fixture
async def mk5():
    reader = asyncio.StreamReader()
    reader.data = None

    def mock_response(data):
        print("sending: ", data)
        if reader.data:
            print("receiving: ", reader.data)
            reader.feed_data(reader.data)

    writer = AsyncMock()
    writer.write = Mock(side_effect=mock_response)
    instance = MK5(MK5Connection(reader, writer))

    def inject_reply(reply: str):
        crl = f"{instance._conn._crl:02x}"
        reply = reply[:3] + crl + reply[5:]
        reader.data = reply.encode()

    instance.inject_reply = inject_reply

    print(instance)
    yield instance
    assert not instance._conn.reader.at_eof()
    assert not instance._conn._runner.done()
    print("done")
    reader.feed_eof()
    instance._conn.stop()
