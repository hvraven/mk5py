Python interface to the MK5 magnetic field stabilisation system made by IDE. 

This package provides an incomplete implementation of the protocol. It was 
created using the manufacturers documentation and partly adjusted to the 
observed deviations in the responses.

Tested using a device with firmwave version 1.2.4